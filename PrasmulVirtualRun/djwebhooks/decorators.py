# -*- coding: utf-8 -*-
from functools import partial
from webhooks.decorators import base_hook
from webhooks.hashes import basic_hash_function
from .senders import orm_callable, redisq_callable, redislog_hook

hook = partial( # The decorator that does all the lifting.
    base_hook,
    sender_callable=orm_callable,
    hash_function=basic_hash_function
)

webhook = hook # the hook function

# Make the redis queue hook work
# This is decorator that does all the lifting.
redisq_hook = partial(
    base_hook,
    sender_callable=redisq_callable,
    hash_function=basic_hash_function
)

# alias the redis_hook function
redisq_webhook = redisq_hook

# alias the redis_hook function
redislog_webhook = redislog_hook