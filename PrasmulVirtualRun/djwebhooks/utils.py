import sys

def always_string(value):
    if sys.version > '3': # 3 as python version
        return value.decode('utf-8')
    return value