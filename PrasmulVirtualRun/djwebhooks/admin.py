# -*- coding: utf-8 -*-
from django.contrib import admin
from django.utils import six

from .models import WebhookTarget, Delivery

class DeliveryAdmin(admin.ModelAdmin):
    if six.PY3:
        exclude = ('payload', )

admin.site.register(Delivery, DeliveryAdmin)
admin.site.register(WebhookTarget)