import os
from django.conf import settings
import datetime
from django.db import models

# Create your models here.
class Credential(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    value = models.CharField(max_length=300)

    def __str__(self):
        return f"{self.user.username} - {self.value}"

class Activity(models.Model):
    id_activity = models.IntegerField(primary_key=True)
    kudos_count=models.IntegerField(null=True)
    distance=models.IntegerField(null=True)
    suffer_score=models.IntegerField(null=True)
    max_heartrate=models.IntegerField(null=True)
    total_elevation_gain=models.IntegerField(null=True)
    type=models.CharField(max_length=100)
    activity_options = (
        ('running','RUNNING'),
        ('walk','WALKING'),
        ('cycling','CYCLING'),
        ('all','ALL')
    )
    activity = models.CharField(max_length=300, choices=activity_options)
    
    def __str__(self):
        return self.activity

class Clubs(models.Model):
    id_clubs=models.IntegerField(null=True)
    name=models.CharField(max_length=300, null=True)
    url=models.URLField
    profile_medium=models.CharField(max_length=300, null=True)

    def __str__(self):
        return str(self.id_clubs)

class Details(models.Model):
    id_details=models.IntegerField(null=True)
    firstname=models.CharField(max_length=100, null=True)
    lastname=models.CharField(max_length=100, null=True)
    profile=models.CharField(max_length=300, null=True)
    chosen_clubs=models.ForeignKey(Clubs, on_delete=models.CASCADE)
    weight=models.IntegerField(null=True)
    city=models.CharField(max_length=100, null=True)
    state=models.CharField(max_length=100, null=True)
    created_at=models.CharField(max_length=100, null=True)
    gender_options = (
        ('male','MALE'),
        ('female','FEMALE')
    )
    gender = models.CharField(max_length=300, choices=gender_options)

    def __str__(self):
        return str(self.id_details)

class Athlete(models.Model):
    user = models.ForeignKey(Credential, on_delete=models.CASCADE, blank=True, null=True)
    starting_point = models.DateTimeField(default=datetime.date)
    finishing_point = models.DateTimeField(default=datetime.date)
    latitude_current = models.CharField(max_length=300, blank=True, null=True),
    longitude_current = models.CharField(max_length=300, blank=True, null=True),
    heart_rate = models.CharField(max_length=300, blank=True, null=True)
    average_speed = models.IntegerField(default=0)
    selected_activity = models.ForeignKey(Activity, on_delete=models.CASCADE, blank=True, null=True)
    def __str__(self):
        return self.user