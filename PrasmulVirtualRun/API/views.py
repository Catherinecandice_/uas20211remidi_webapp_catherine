from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from .forms import NewUserForm
from django.contrib.auth import login, authenticate, logout
from django.contrib import messages
from rest_framework import viewsets
from API.serializers import UserSerializer
import requests
from django.contrib.auth.forms import AuthenticationForm 

# Create your views here.
def handlingAutho(request):
    # Get the url's of Strava Auth system
    code = request.GET.get('code', '')# <-- masukan ini ke DB untuk di simpen
    print('GET AUTHORIZATION')
    print(code)
    url_strava = 'https://www.strava.com/oauth/token?client_id=76095&client_secret=2ee9a4a53e4359667f52a4283fdbe6edfe6df50e&code=3eab256b8c169e1011c7ccd5ccd1618865b00d7f&grant_type=authorization_code'
    x = requests.post(url_strava)
    print('POST AUTHORIZATION') # data response from Strava
    print(x.text)
    responses = x.json()
    access_token = responses['003d836ee5350154683f70def3cdfc8582f6675b'] # <-- masukan ini ke DB untuk di simpen
    refresh_token = responses['81e40c613d13e1a6bcc33d66599da7bd86b6594a'] # <-- masukan ini ke DB untuk di simpen
    print(access_token)

    url_get_activities = 'https://www.strava.com/api/v3/athlete/activities?access_token=003d836ee5350154683f70def3cdfc8582f6675b'
    x = requests.get(url_get_activities) # <-- masukan ini ke DB untuk di simpen
    print('AUTHORIZATION COMPLETED')
    print(x.text)
    # masukan ke DB
    content = ''
    if code == '':
        content = ''
    else:
        content = 'AUTHORIZATION CONNECTED'
    context = {'status': content}
    return render(request, 'authostrava.html', context)

# ViewSets define the view behavior.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer

# Define user register 
def register_request(request):
	if request.method == "POST":
		form = NewUserForm(request.POST)
		if form.is_valid():
			user = form.save()
			login(request, user)
			messages.success(request, "Registration successful." )
			return redirect("main:homepage")
		messages.error(request, "Unsuccessful registration. Invalid information.")
	form = NewUserForm()
	return render (request=request, template_name="main/register.html", context={"register_form":form})

# Define user login
def login_request(request):
	if request.method == "POST":
		form = AuthenticationForm(request, data=request.POST)
		if form.is_valid():
			username = form.cleaned_data.get('username')
			password = form.cleaned_data.get('password')
			user = authenticate(username=username, password=password)
			if user is not None:
				login(request, user)
				messages.info(request, f"You are now logged in as {username}.")
				return redirect("main:homepage")
			else:
				messages.error(request,"Invalid username or password.")
		else:
			messages.error(request,"Invalid username or password.")
	form = AuthenticationForm()
	return render(request=request, template_name="main/login.html", context={"login_form":form})

# Define user logout
def logout_request(request):
	logout(request)
	messages.info(request, "You have successfully logged out.") 
	return redirect("main:homepage")

