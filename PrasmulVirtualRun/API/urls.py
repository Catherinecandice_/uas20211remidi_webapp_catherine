from rest_framework import routers
from django.urls import path, include
from django.contrib.auth.models import User
from API.views import UserViewSet
from . import views
import templates 

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'users', UserViewSet)

app_name = "main"  
# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    path('', include(router.urls)),
    path('', views.homepage, name="homepage"),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path("register", views.register_request, name="register"),
    path("login", views.login_request, name="login"),
    path("logout", views.logout_request, name= "logout"),
]
